﻿$Help = Get-Help -Name "Get-Help"

$FilePath = "$env:TEMP\Get-Help.html"

$Help |
ConvertTo-Html |
Set-Content -Path $FilePath

Invoke-Item -Path $FilePath
