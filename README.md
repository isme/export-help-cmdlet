# Export-Help

PowerShell's online and interactive help system is one of its best features.

That said, I don't find it the most comfortable experience to read documentation in a console.

Instead I prefer to view help in a web browser.

The browser's built-in search functionality is better.

This is where PowerShell's object-orientation is actually a hindrance rather than a help. Conceptually, help is prose. Text. I should be able to use text-processing techniques to find the part I need. I could maybe hack something together using Where clauses or Select-String, but that already feels like too much work.

Windows lacks a good built-in pager. less.exe is only a partial solution.

Proportional font is more pleasant for reading prose.

The browser has proper word wrap. The console does wrap long lines, but the soft break can occur anywhere in the output message, resulting in awkward-looking text.

For the MS-shipped cmdlets, this is easy, because all the documentation is on MSDN.

How do I create my own HTML versions of the help?

What is PowerShell's equivalent of javadoc? 

"Project Avocado"
